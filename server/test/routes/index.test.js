import { Router } from 'express';

describe('Router', () => {
    let router;
    let getFilesMock;

    beforeEach(() => {
        router = Router();
        getFilesMock = (done) => {
            done();
        };
    });

    it('should call getFilesData controller', (done) => {
        router.get('/files/data', getFilesMock(done));
        router.handle({ method: 'GET', url: '/files/data' }, {}, () => { });
    });

    it('should call getFilesList controller', (done) => {
        router.get('/files/list', getFilesMock(done));
        router.handle({ method: 'GET', url: '/files/list' }, {}, () => { });
    });
});