import { expect } from 'chai';
import api from '../../src/api/index.js';

describe('API', () => {
    it('should have the correct base URL', () => {
        expect(api.defaults.baseURL).to.equal('https://echo-serv.tbxnet.com');
    });

    it('should have a timeout of 10000', () => {
        expect(api.defaults.timeout).to.equal(10000);
    });

    it('should have the correct authorization header', () => {
        expect(api.defaults.headers.Authorization).to.equal('Bearer aSuperSecretKey');
    });
});