import { expect } from 'chai';
import asyncHandler from '../../src/middlewares/async_handler.js';

describe('AsyncHandler', () => {
    it('should call next with an error if the function throws', async () => {
        const error = new Error('test error');
        const fn = () => { throw error; };
        const next = (err) => { throw err; };

        try {
            await asyncHandler(fn)({}, {}, next);
        } catch (err) {
            expect(err).to.equal(error);
        }
    });

    it('should not call next with an error if the function does not throw', async () => {
        const fn = () => { };
        const next = (err) => { throw err; };

        try {
            await asyncHandler(fn)({}, {}, next);
        } catch (err) {
            throw new Error('next should not have been called with an error');
        }
    });
});