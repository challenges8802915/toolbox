import { expect } from 'chai';
import express from 'express';
import api from '../../src/api/index.js';

const router = express.Router();

const generateTestSuite = (endpoint) => {
    describe(`GET ${endpoint}`, () => {
        let server;
        const getApp = () => {
            const app = express();
            app.use(router);
            app.get(endpoint, (req, res) => {
                res.status(200).json({ message: 'ok' });
            });
            return app;
        }

        const startServer = (app, port) => {
            return new Promise((resolve, reject) => {
                server = app.listen(port, resolve).on('error', reject);
            });
        };

        afterEach(() => {
            if (server) {
                server.close();
                server = undefined;
            }
        });

        it('should return json data', async () => {
            const app = getApp();
            const port = 4002;
            await startServer(app, port);
            const res = await api.get(`http://localhost:${port}${endpoint}`);
            expect(res.status).to.equal(200);
            expect(res.data.message).to.equal('ok');
        });
    });
}

generateTestSuite('/files/data');
generateTestSuite('/files/list');