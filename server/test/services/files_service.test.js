import { expect } from 'chai';
import FilesService from '../../src/services/files_service.js';

describe('FilesService', () => {
  let service;
  let consoleError;

  beforeEach(() => {
    const api = {
      get: () => Promise.resolve({ data: 'file' })
    };
    service = FilesService.create({ api });
    consoleError = console.error;
    console.error = () => { };
  });

  afterEach(() => {
    console.error = consoleError;
  });

  it('should return corrupted file if no valid lines', async () => {
    const api = {
      get: () => Promise.resolve({ data: '' })
    };
    service = FilesService.create({ api });
    const files = await service.fetchFile('test.csv');
    expect(files).to.deep.equal([
      {
        file: 'test.csv',
        lines: 'Corrupted file'
      }
    ]);
  });

  it('should throw error if fetching files fails', async () => {
    const api = {
      get: () => Promise.reject('Error')
    };
    service = FilesService.create({ api });
    try {
      await service.fetchFiles();
    } catch (error) {
      expect(error).to.equal('Error');
    }
  });


  it('should process files and not filter invalid ones', async () => {
    const files = await service.processFiles(['file1', 'file2'], false);
    expect(files).to.deep.equal([
      {
        file: 'file1',
        lines: []
      },
      {
        file: 'file2',
        lines: []
      }
    ]);
  });

});