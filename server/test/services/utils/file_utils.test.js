import { expect } from 'chai';
import { isValidLine, transformLineToObject, downloadFile, getFiles, handleApiError } from '../../../src/services/utils/files_utils.js';

describe('Utils', () => {
    let api;

    beforeEach(() => {
        api = {
            get: () => Promise.resolve({ data: 'file' })
        };
    });

    describe('isValidLine', () => {
        it('should return false if index is 0', () => {
            expect(isValidLine('test,1,abcdef1234567890abcdef1234567890', 0)).to.be.false;
        });

        it('should return false if number or hex is missing', () => {
            expect(isValidLine('test,,abcdef1234567890abcdef1234567890', 1)).to.be.false;
            expect(isValidLine('test,1,', 1)).to.be.false;
        });

        it('should return true if line is valid', () => {
            expect(isValidLine('test,1,2,abcdef1234567890abcdef1234567890', 1)).to.be.true;
        });
    });

    describe('transformLineToObject', () => {
        it('should transform line to object', () => {
            const result = transformLineToObject('test,hello,1,abcdef1234567890abcdef1234567890');
            expect(result).to.deep.equal({
                text: 'hello',
                number: 1,
                hex: 'abcdef1234567890abcdef1234567890'
            });
        });
    });

    describe('downloadFile', () => {
        it('should call handleApiError with correct arguments', async () => {
            const result = await downloadFile(api, 'test.csv');
            expect(result).to.equal('file');
        });
    });

    describe('getFiles', () => {
        it('should return files from api', async () => {
            api = {
                get: () => Promise.resolve({ data: { files: ['file1', 'file2'] } })
            };
            const result = await getFiles(api);
            expect(result).to.deep.equal(['file1', 'file2']);
        });
    });

    describe('handleApiError', () => {
        it('should return data if api call is successful', async () => {
            const apiCall = Promise.resolve({ data: 'data' });
            const result = await handleApiError(apiCall, 'Error message');
            expect(result).to.equal('data');
        });

        it('should return null and log error if api call fails', async () => {
            const apiCall = Promise.reject('Error');
            const consoleStub = console.error;
            console.error = () => { };
            const result = await handleApiError(apiCall, 'Error message');
            expect(result).to.be.null;
            console.error = consoleStub;
        });
    });
});
