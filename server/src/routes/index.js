import { Router } from "express";
import { getFilesData, getFilesList } from "../controllers/index.js"

const router = Router();

router.get('/files/data', getFilesData);
router.get('/files/list', getFilesList);

export default router;