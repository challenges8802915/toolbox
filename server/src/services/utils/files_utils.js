const SOURCE_ENDPOINT = '/v1/secret';

export const isValidLine = (line, index) => {
    if (index === 0) return false;

    const [, , number, hex] = line.split(',');

    if (!number || !hex) return false;

    const isNumber = /^-?[0-9]+(\.[0-9]+)?$/.test(number.trim());
    const isHex = /^[\da-f]{32}$/i.test(hex.trim());

    return isNumber && isHex;
}

export const transformLineToObject = (line) => {
    const [, text, number, hex] = line.split(',');

    return {
        text,
        number: Number(number),
        hex
    };
}

export const downloadFile = async (api, fileName) => {
    return await handleApiError(
        api.get(`${SOURCE_ENDPOINT}/file/${fileName}`),
        `Error downloading the file ${fileName}:`
    );
}

export const getFiles = async (api) => {
    const data = await handleApiError(
        api.get(`${SOURCE_ENDPOINT}/files`),
        'Error searching for files:'
    );

    return data ? data.files : [];
}

export const handleApiError = async (apiCall, errorMessage) => {
    try {
        const { data } = await apiCall;
        return data;
    } catch (error) {
        console.error(errorMessage, error);
        return null;
    }
}