import { isValidLine, transformLineToObject, downloadFile, getFiles } from './utils/files_utils.js';

class FilesService {
  constructor({ api }) {
    this.api = api;
  }

  static create({ api }) {
    return new FilesService({ api });
  }

  async fetchFiles() {
    try {
      const filesNames = await getFiles(this.api);
      const files = await this.processFiles(filesNames, true);
      return files;
    } catch (error) {
      console.error('Error gathering and cleaning all files:', error);
      throw error;
    }
  }

  async fetchFile(fileName) {
    try {
      const files = await this.processFiles([fileName], false);

      if (files.length === 0 || files[0].lines.length === 0) {
        console.error(`The file ${fileName} has no valid lines.`);
        return [{
          file: fileName,
          lines: "Corrupted file"
        }];
      }

      return files;
    } catch (error) {
      console.error('Error gathering all files:', error);
      throw error;
    }
  }

  async processFiles(filesNames, filterInvalidFiles = false) {
    const promises = filesNames.map(file =>
      downloadFile(this.api, file).then(data => ({ fileName: file, data }))
    );

    const files = await Promise.allSettled(promises);

    let validFiles = files
      .filter(result => result.status === 'fulfilled' && result.value !== null)
      .map((result) => {
        const lines = result.value.data ? result.value.data.split('\n') : [];
        const cleanedData = lines.filter(isValidLine);
        const cleanedDataObjects = cleanedData.map(transformLineToObject);

        return {
          file: result.value.fileName,
          lines: cleanedDataObjects
        };
      });

    if (filterInvalidFiles) {
      validFiles = validFiles.filter(file => file.lines.length > 0);
    }

    return validFiles;
  }
}

export default FilesService;