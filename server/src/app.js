import express from 'express';
import filesRouter from './routes/index.js';
import cors from 'cors';

const app = express();

app.use(cors());
app.use(filesRouter);

app.get('/', (req, res) => {
    res.send('Welcome!');
});

app.use((req, res) => {
    res.status(404).send(`Sorry, we couldn't find that!`);
});

app.use((err, req, res, next) => {
    console.error('Error handling route \'/\':', err);
    res.status(500).json({ error: 'There was an error processing the files' });
});

export default app;