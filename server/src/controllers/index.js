import { Router } from "express";
import FilesService from '../services/files_service.js';
import api from '../api/index.js';
import asyncHandler from '../middlewares/async_handler.js';
import { getFiles } from '../services/utils/files_utils.js';

const router = Router();

export const getFilesData = asyncHandler(async (req, res) => {
    const service = FilesService.create({ api });
    const fileName = req.query.fileName;
    const fetchMethod = fileName ? 'fetchFile' : 'fetchFiles';
    const cleanedData = await service[fetchMethod](fileName);
    res.status(200).json(cleanedData);
});

export const getFilesList = asyncHandler(async (req, res) => {
    const data = await getFiles(api);
    res.status(200).json(data);
});

export default router;