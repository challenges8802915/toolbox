import axios from "axios";

const API_KEY = 'Bearer aSuperSecretKey';
const BASE_URL = 'https://echo-serv.tbxnet.com';

const api = axios.create({
  baseURL: BASE_URL,
  timeout: 10000,
  headers: {
    Authorization: API_KEY,
  },
});

export default api;