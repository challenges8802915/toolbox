# Format Files

![Logo](https://s3.amazonaws.com/resumator/customer_20180208142221_FSXIX0H666MNM11B/logos/20180224185018_logo_toolbox.jpg)


## Project Description
This Full Stack project consists of two parts:

- **REST API**: Developed using Node.js and Express, and tested using the Mocha and Chai libraries for API testing.

- **Frontend Client**: Developed with React, Redux, and Bootstrap, with tests implemented using the Jest library to ensure the robustness and quality of the frontend.

## How Can I Run the Program?
You'll need to have [NodeJS](https://nodejs.org/). It is recommended to use Node.js 14 or a higher version to ensure compatibility. Additionally, if you wish to run it using Docker, you will need Docker installed on your system.

1. Run Docker Compose or the start.cmd file
Option A: Docker Compose: `docker-compose up --build`
Option B: start.cmd file: `./start.cmd`

2. Access the application
The service starts locally at the following URL: http://localhost:3000/

## How to Run the Tests
The tests for both the REST API and the frontend client are run using the same command, `npm test`, but in different directories.

1. Navigate to the directory /server or /client in your terminal.
2. Install dependencies: `npm install`
3. To run the tests, execute: `npm test`

## Information

The API is responsible for fetching information from an external API and reformatting it.
Example of the content of a file:

```javascript

  file,text,number,hex
  test2.csv,ZLzvJ
  test2.csv,DbsEXpWtkWmxHoMvXryDbry,72,edc16047d15e4121ccfab911f1b6e71d

```

Example response after formatting the file:

```javascript
{
    "file": "test2.csv",
    "lines": [
        {
            "text": "DbsEXpWtkWmxHoMvXryDbry",
            "number": 72,
            "hex": "edc16047d15e4121ccfab911f1b6e71d"
        }
    ]
}
```

A layout is created using React + Redux + React Bootstrap, where you can query healthy files. You can also filter by fileName.
