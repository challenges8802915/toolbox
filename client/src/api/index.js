import axios from "axios";

export const api = axios.create({
    baseURL: "http://localhost:4000"
});

export const getAllFiles = async () => {
    try {
        const response = await api.get("/files/data");
        if (response.status === 200) {
            return response.data;
        } else {
            throw new Error(`Error fetching all files: Status code ${response.status}`);
        }
    } catch (error) {
        throw error;
    }
};

export const getFileByName = async (fileName) => {
    try {
        const response = await api.get("/files/data", {
            params: {
                fileName: fileName
            }
        });
        if (response.status === 200) {
            return response.data;
        } else {
            throw new Error(`Error fetching file by name: ${fileName} - Status code ${response.status}`);
        }
    } catch (error) {
        throw error;
    }
};