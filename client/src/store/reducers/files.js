import { createSlice, createAsyncThunk } from '@reduxjs/toolkit';
import { getAllFiles, getFileByName } from '../../api/index.js';


export const fetchAllFiles = createAsyncThunk('files/fetchAll', async () => {
    return await getAllFiles();
});

export const fetchFileByName = createAsyncThunk('files/fetchByName', async (fileName) => {
    return await getFileByName(fileName);
});

const filesSlice = createSlice({
    name: 'files',
    initialState: { files: [], file: [], statusAllFiles: 'idle', statusFileByName: 'idle', error: null, selectedFile: null },
    reducers: {
        selectFile: (state, action) => {
            state.selectedFile = action.payload || null;
        },
    },
    extraReducers: (builder) => {
        builder
            .addCase(fetchAllFiles.pending, (state, action) => {
                state.statusAllFiles = 'loading';
            })
            .addCase(fetchFileByName.pending, (state, action) => {
                state.statusFileByName = 'loading';
            })
            .addCase(fetchAllFiles.fulfilled, (state, action) => {
                state.statusAllFiles = 'succeeded';
                state.files = action.payload;
            })
            .addCase(fetchFileByName.fulfilled, (state, action) => {
                state.statusFileByName = 'succeeded';
                state.file = action.payload;
            })
            .addCase(fetchAllFiles.rejected, (state, action) => {
                state.statusAllFiles = 'failed';
                state.error = action.error.message;
            })
            .addCase(fetchFileByName.rejected, (state, action) => {
                state.statusFileByName = 'failed';
                state.error = action.error.message;
            });
    },
});

export const { selectFile } = filesSlice.actions;

export const selectFiles = state => state.files.files;
export const selectSelectedFile = state => state.files.selectedFile;
export const selectStatusAllFiles = state => state.files.statusAllFiles;
export const selectCurrentFile = state => state.files.file;

export default filesSlice.reducer;