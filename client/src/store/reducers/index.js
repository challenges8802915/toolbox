import { combineReducers } from 'redux';
import filesReducer from './files.js';

const rootReducer = combineReducers({
    files: filesReducer,
});

export default rootReducer;