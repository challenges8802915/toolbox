import React from 'react';
import IndexPage from './pages/index.jsx'

const App = () => {
    return (
        <IndexPage />
    );
}

export default App;