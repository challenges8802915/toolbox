import React from 'react';
import CustomTable from '../components/customTable.jsx';
import LoadSpinner from '../utilities/loadSpinner.jsx';
import FilterFiles from '../components/filterFiles.jsx';
import { useFiles } from '../hooks/useFiles.js'
import { useSearchFile } from '../hooks/useSearchFile.js'


const IndexPage = () => {
    const { statusAllFiles, filteredFiles } = useFiles();
    const { handleFileSelect, file } = useSearchFile();

    return (
        <div className='p-4'>
            {statusAllFiles === 'loading' ? (
                <LoadSpinner />
            ) : (
                <CustomTable
                    title="Search files"
                    headers={["File Name", "Text", "Number", "Hex"]}
                    rows={filteredFiles.flatMap(item => item.lines.map(line => [item.file, line.text, line.number, line.hex]))}
                    itemsFilter={<FilterFiles handleFileSelect={handleFileSelect} file={file} />}
                />
            )}
        </div>
    );
};

export default IndexPage;