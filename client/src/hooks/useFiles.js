import { useDispatch, useSelector } from 'react-redux';
import { useEffect, useCallback, useMemo } from 'react';
import { fetchAllFiles, selectFiles, selectSelectedFile, selectStatusAllFiles } from '../store/reducers/files.js';

export const useFiles = () => {
    const dispatch = useDispatch();
    const files = useSelector(selectFiles);
    const selectedFile = useSelector(selectSelectedFile);
    const statusAllFiles = useSelector(selectStatusAllFiles);

    const fetchData = useCallback(() => {
        if (statusAllFiles === 'idle') {
            try {
                dispatch(fetchAllFiles());
            } catch (error) {
                console.error('Failed to fetch files: ', error);
            }
        }
    }, [dispatch, statusAllFiles]);

    useEffect(() => {
        fetchData();
    }, [fetchData]);

    const filteredFiles = useMemo(() => {
        return selectedFile
            ? files.filter(file => file.file === selectedFile)
            : files;
    }, [files, selectedFile]);

    return { statusAllFiles, filteredFiles };
}


