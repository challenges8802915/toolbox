import { useDispatch, useSelector } from 'react-redux';
import { useEffect, useCallback } from 'react';
import { fetchFileByName, selectFile, selectCurrentFile } from '../store/reducers/files.js';

export const useSearchFile = () => {
    const dispatch = useDispatch();
    const file = useSelector(selectCurrentFile);

    const fetchFile = useCallback((fileName) => {
        try {
            dispatch(fetchFileByName(fileName));
        } catch (error) {
            console.error('Failed to fetch file: ', error);
        }
    }, [dispatch]);

    const handleFileSelect = (event) => {
        dispatch(selectFile(event.target.value));
    }

    useEffect(() => {
        fetchFile();
    }, [fetchFile]);

    return { handleFileSelect, file };
}