import React from 'react';
import Spinner from 'react-bootstrap/Spinner';

const LoadSpinner = () => {
    return (
        <Spinner animation="border" role="status" data-testid="load-spinner">
            <span className="visually-hidden">Loading...</span>
        </Spinner>
    );
}

export default LoadSpinner;