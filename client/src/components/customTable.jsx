import React from 'react';
import Table from 'react-bootstrap/Table';


const CustomTable = ({ title, headers, rows, itemsFilter }) => {
    return (
        <div data-testid="custom-table">
            <h2>{title}</h2>
            <div className='pb-2'>{itemsFilter}</div>
            <Table responsive bordered hover striped>
                <thead>
                    <tr>
                        {headers.map((header) => (
                            <th key={header}>{header}</th>
                        ))}
                    </tr>
                </thead>
                <tbody>
                    {rows.map((row) => (
                        <tr key={row}>
                            {row.map((cell) => (
                                <td key={cell}>{cell}</td>
                            ))}
                        </tr>
                    ))}
                </tbody>
            </Table>
        </div>
    );
};

export default CustomTable;