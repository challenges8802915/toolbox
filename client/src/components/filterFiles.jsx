import React from 'react';
import Form from 'react-bootstrap/Form';


const FilterFiles = ({ handleFileSelect, file }) => {
    return (
        <Form.Select size="lg" onChange={handleFileSelect}>
            <option value="">All Files</option>
            {file.map((fileItem) => (
                <option key={fileItem.file} value={fileItem.file}>{fileItem.file}</option>
            ))}
        </Form.Select>
    );
};

export default FilterFiles;