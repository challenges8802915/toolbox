import store from '../../src/store/index';
import { selectFile } from '../../src/store/reducers/files';

describe('store', () => {
    it('should handle actions', () => {
        const selectedFile = 'testFile';
        const action = selectFile(selectedFile);
        store.dispatch(action);
        const state = store.getState();
        expect(state.files.selectedFile).toEqual(selectedFile);
    });
});