import rootReducer from '../../../src/store/reducers/index';
import filesReducer, { selectFile } from '../../../src/store/reducers/files';

describe('rootReducer', () => {
    const initialState = {
        files: filesReducer(undefined, {}),
    };

    it('should return the initial state', () => {
        expect(rootReducer(undefined, {})).toEqual(initialState);
    });

    it('should handle files actions', () => {
        const selectedFile = 'testFile';
        const action = selectFile(selectedFile);
        const expectedState = {
            files: filesReducer(undefined, action),
        };
        expect(rootReducer(undefined, action)).toEqual(expectedState);
    });
});