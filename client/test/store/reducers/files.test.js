import filesReducer, { selectFile, fetchAllFiles, fetchFileByName } from '../../../src/store/reducers/files';

describe('Files reducer', () => {
    const initialState = {
        files: [],
        file: [],
        statusAllFiles: 'idle',
        statusFileByName: 'idle',
        error: null,
        selectedFile: null
    };

    it('should handle initial state', () => {
        expect(filesReducer(undefined, {})).toEqual(initialState);
    });

    it('should handle selectFile', () => {
        const selectedFile = 'testFile';
        const expectedState = { ...initialState, selectedFile };
        expect(filesReducer(initialState, selectFile(selectedFile))).toEqual(expectedState);
    });

    it('should handle fetchAllFiles.fulfilled', async () => {
        const mockFiles = ['file1', 'file2'];
        const expectedState = { ...initialState, files: mockFiles, statusAllFiles: 'succeeded' };
        const action = { type: fetchAllFiles.fulfilled.type, payload: mockFiles };
        expect(filesReducer(initialState, action)).toEqual(expectedState);
    });

    it('should handle fetchFileByName.fulfilled', async () => {
        const mockFile = 'file1';
        const expectedState = { ...initialState, file: mockFile, statusFileByName: 'succeeded' };
        const action = { type: fetchFileByName.fulfilled.type, payload: mockFile };
        expect(filesReducer(initialState, action)).toEqual(expectedState);
    });
});