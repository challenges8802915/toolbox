import React from 'react';
import { render, waitFor } from '@testing-library/react';
import IndexPage from '../../src/pages/index';
import { useFiles } from '../../src/hooks/useFiles';
import { useSearchFile } from '../../src/hooks/useSearchFile';

jest.mock('../../src/hooks/useFiles');
jest.mock('../../src/hooks/useSearchFile');

describe('IndexPage', () => {
    it('renders LoadSpinner when statusAllFiles is loading', async () => {
        useFiles.mockReturnValue({
            statusAllFiles: 'loading',
            filteredFiles: []
        });

        useSearchFile.mockReturnValue({
            handleFileSelect: jest.fn(),
            file: []
        });

        const { getByTestId } = render(<IndexPage />);
        await waitFor(() => expect(getByTestId('load-spinner')).toBeInTheDocument());
    });

    it('renders CustomTable when statusAllFiles is not loading', async () => {
        useFiles.mockReturnValue({
            statusAllFiles: 'loaded',
            filteredFiles: [
                {
                    file: 'testFile',
                    lines: [
                        {
                            text: 'testText',
                            number: 'testNumber',
                            hex: 'testHex'
                        }
                    ]
                }
            ]
        });

        useSearchFile.mockReturnValue({
            handleFileSelect: jest.fn(),
            file: []
        });

        const { getByTestId } = render(<IndexPage />);
        await waitFor(() => expect(getByTestId('custom-table')).toBeInTheDocument());
    });
});