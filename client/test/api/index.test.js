import { getAllFiles, getFileByName } from '../../src/api/index'

jest.mock('../../src/api/index');

describe('API tests', () => {
    beforeEach(() => {
        getAllFiles.mockClear();
        getFileByName.mockClear();
    });

    test('getAllFiles returns data successfully', async () => {
        const data = { data: ['file1', 'file2'] };
        getAllFiles.mockResolvedValue(data);

        const response = await getAllFiles();

        expect(response).toEqual(data);
        expect(getAllFiles).toHaveBeenCalledTimes(1);
    });

    test('getFileByName returns data successfully', async () => {
        const fileName = 'file1';
        const data = { data: { name: 'file1', content: 'content' } };
        getFileByName.mockResolvedValue(data);

        const response = await getFileByName(fileName);

        expect(response).toEqual(data);
        expect(getFileByName).toHaveBeenCalledWith(fileName);
    });
});