import React from 'react';
import { render, screen } from '@testing-library/react';
import LoadSpinner from '../../src/utilities/loadSpinner';

describe('LoadSpinner', () => {
    test('renders correctly and displays the spinner', () => {
        render(<LoadSpinner />);

        const spinnerElement = screen.getByTestId('load-spinner');
        expect(spinnerElement).toBeInTheDocument();
    });
});