import { waitFor, renderHook } from '@testing-library/react';
import { useFiles } from '../../src/hooks/useFiles';
import { useDispatch, useSelector } from 'react-redux';
import { selectFiles, selectSelectedFile, selectStatusAllFiles } from '../../src/store/reducers/files';

jest.mock('react-redux', () => ({
    useDispatch: jest.fn(),
    useSelector: jest.fn(),
}));

describe('useFiles hook', () => {
    let mockDispatch;

    beforeEach(() => {
        mockDispatch = jest.fn();
        useDispatch.mockReturnValue(mockDispatch);
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    it('should fetch all files and return filtered files', async () => {
        const files = [{ file: 'file1' }, { file: 'file2' }];
        const selectedFile = 'file1';
        const statusAllFiles = 'idle';

        useSelector.mockImplementation(selector => {
            if (selector === selectFiles) return files;
            if (selector === selectSelectedFile) return selectedFile;
            if (selector === selectStatusAllFiles) return statusAllFiles;
        });

        const { result } = renderHook(() => useFiles());

        await waitFor(() => expect(mockDispatch).toHaveBeenCalled());

        expect(result.current.statusAllFiles).toEqual(statusAllFiles);
        expect(result.current.filteredFiles).toEqual([{ file: 'file1' }]);
    });
});