import { waitFor, renderHook } from '@testing-library/react';
import { useSearchFile } from '../../src/hooks/useSearchFile';
import { useDispatch, useSelector } from 'react-redux';
import { fetchFileByName, selectFile } from '../../src/store/reducers/files';

jest.mock('react-redux', () => ({
    useDispatch: jest.fn(),
    useSelector: jest.fn(),
}));

describe('useSearchFile hook', () => {
    let mockDispatch;

    beforeEach(() => {
        mockDispatch = jest.fn();
        useDispatch.mockReturnValue(mockDispatch);
    });

    afterEach(() => {
        jest.clearAllMocks();
    });

    it('should fetch file by name and handle file select', async () => {
        const fileName = 'file1';
        const file = { file: fileName };
        useSelector.mockReturnValue(file);

        const { result } = renderHook(() => useSearchFile());

        await waitFor(() => {
            const dispatchedFunction = mockDispatch.mock.calls[0][0];
            expect(typeof dispatchedFunction).toBe('function');
            expect(dispatchedFunction.name).toBe(fetchFileByName().name);
        });

        const event = { target: { value: fileName } };
        result.current.handleFileSelect(event);

        expect(mockDispatch).toHaveBeenCalledWith(selectFile(fileName));
        expect(result.current.file).toEqual(file);
    });
});