import CustomTable from '../../src/components/customTable';
import React from 'react';
import { render, screen } from '@testing-library/react';

describe('CustomTable', () => {
    const title = 'Test Title';
    const headers = ['Header 1', 'Header 2'];
    const rows = [['Cell 1', 'Cell 2'], ['Cell 3', 'Cell 4']];
    const itemsFilter = <div>Filter</div>;

    beforeEach(() => {
        render(<CustomTable title={title} headers={headers} rows={rows} itemsFilter={itemsFilter} />);
    });

    test('renders correctly', () => {
        expect(screen.getByText(title)).toBeInTheDocument();
        expect(screen.getByText('Filter')).toBeInTheDocument();

        headers.forEach(header => {
            expect(screen.getByText(header)).toBeInTheDocument();
        });

        rows.forEach(row => {
            row.forEach(cell => {
                expect(screen.getByText(cell)).toBeInTheDocument();
            });
        });
    });
});