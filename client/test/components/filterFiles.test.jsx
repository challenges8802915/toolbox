import React from 'react';
import { render, fireEvent } from '@testing-library/react';
import FilterFiles from '../../src/components/filterFiles';


describe('FilterFiles', () => {
    const handleFileSelectMock = jest.fn();
    const fileMock = [
        { file: 'file1' },
        { file: 'file2' },
        { file: 'file3' },
    ];

    it('renders without crashing', () => {
        render(<FilterFiles handleFileSelect={handleFileSelectMock} file={fileMock} />);
    });

    it('calls handleFileSelect when an option is selected', () => {
        const { getByRole } = render(<FilterFiles handleFileSelect={handleFileSelectMock} file={fileMock} />);
        const select = getByRole('combobox');
        fireEvent.change(select, { target: { value: 'file1' } });
        expect(handleFileSelectMock).toHaveBeenCalled();
    });

    it('renders the correct number of options', () => {
        const { getAllByRole } = render(<FilterFiles handleFileSelect={handleFileSelectMock} file={fileMock} />);
        const options = getAllByRole('option');
        expect(options.length).toBe(fileMock.length + 1);
    });
});